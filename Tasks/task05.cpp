//
// Created by Ostrichbeta Chan on 2022/03/24.
// Task 05: Josephus's Ring
// Description: Use appropriate algorithms to solve any Josephus's ring question.
//

#include <vector>
#include <iostream>

long josephusRing(long circumference, long target, bool doYouWannaHereHowICalculateTheResultIfSoMakeMeTrueOtherKeepMeFalseWhichIsAlsoTheDefaultValue = false) {
    // Circumference stands for the amount of elements in the ring, target stands for the interval to kick out the people,
    // as for the last argument, if enabled, the process will be outputted to stdout.

    // Make necessary checks
    if (circumference <= 0 || target <= 0) throw std::range_error("What the heck did you passed?");

    std::vector<long> playerList(circumference);
    for (long i = 1; i <= circumference; ++i) {
        playerList[i - 1] = i;
    }

    unsigned long currentPos = 0;
    // Start kicking
    while(playerList.size() > 1) {
        unsigned long targetPos = (currentPos + target - 1) % playerList.size();
        if (doYouWannaHereHowICalculateTheResultIfSoMakeMeTrueOtherKeepMeFalseWhichIsAlsoTheDefaultValue) {
            std::cout << playerList[targetPos] << " will be popped!" << std::endl;
        }
        playerList.erase(playerList.begin() + targetPos);
        currentPos = targetPos;
    }

    return playerList[0];
}

void task05Entry(){
    std::cout << josephusRing(41, 3, true) << std::endl;
}

