//
// Created by Ostrichbeta Yick-Ming Chan on 3/22/22.
//

#ifndef WHUDATASTRUCTUREEXP04_TASK01_H
#define WHUDATASTRUCTUREEXP04_TASK01_H

#include "../TaskTemplate.h"

void task01Entry();
const TaskTemplate task01("Sequenced List Library", "Make your own sequenced list library on your own.", task01Entry);

#endif //WHUDATASTRUCTUREEXP04_TASK01_H
