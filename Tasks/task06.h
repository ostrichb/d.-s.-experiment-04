//
// Created by Ostrichbeta Chan on 2022/03/30.
//

#ifndef WHUDATASTRUCTUREEXP04_TASK06_H
#define WHUDATASTRUCTUREEXP04_TASK06_H

#include "../TaskTemplate.h"
#include <iostream>

void task06Entry();
const TaskTemplate task06("Headless Linked List", "Create a headless linked list.", task06Entry);

namespace t6 {
    // Define a single node
    template<typename T>
    class ListNode {
    public:
        T data;
        ListNode<T> *next;
        ListNode(T element, ListNode<T> *nextPtr) : data(element), next(nextPtr) {};
    };

    // Define the whole class
    template<typename T>
    class RoundedLinkedList {
    private:
        ListNode<T> *_entry = nullptr;
        size_t _count = 0;
    public:
        // Constructors
        RoundedLinkedList(); // Empty Constructor
        RoundedLinkedList(size_t size, T element); // Multiplier

        // Destructor
        ~RoundedLinkedList();

        // Getters
        ListNode<T> getEntry();
        size_t size();

        // Setters
        T pop(); // Follow the Python's rule, when popping an element, the value will be returned.
        void insert(size_t pos, T element);
        void remove(size_t pos);
        void push_back(T element);
        void push_front(T element);
        void remove_back();
        void remove_front();

        // Converters
        std::vector<T> toVector();
    };

    template<typename T>
    RoundedLinkedList<T>::RoundedLinkedList() {
        _count = 0;
        _entry = nullptr;
    }

    template<typename T>
    RoundedLinkedList<T>::~RoundedLinkedList() {
        while (_count > 0) this->pop();
    }

    template<typename T>
    void RoundedLinkedList<T>::push_back(T element) {
        if (_count == 0) {
            // If empty, create a new one
            _entry = new ListNode<T>(element, nullptr);
            _entry->next = _entry; // Make a circle
        } else {
            ListNode<T> *currentNode = _entry;
            for (size_t i = 0; i < _count - 1; ++i) {
                currentNode = currentNode->next;
            }
            // CurrentNode now reaches the very end of the list.
            auto *newElement = new ListNode<T>(element, _entry);
            currentNode->next = newElement;
        }
        _count++;
    }

    template<typename T>
    void RoundedLinkedList<T>::push_front(T element) {
        if (_count == 0) push_back(element); else {
            auto *newElement = new ListNode<T>(element, _entry);
            ListNode<T> *currentNode = _entry;
            for (size_t i = 0; i < _count + 1; ++i) {
                currentNode = currentNode->next;
            }
            // CurrentNode now reaches the very end of the list.
            currentNode->next = newElement;
            _entry = newElement; // Move the entry pointer to the newly created element
            _count++;
        }
    }

    template<typename T>
    RoundedLinkedList<T>::RoundedLinkedList(size_t size, T element) {
        for (size_t i = 0; i < size; ++i) {
            this->push_back(element);
        }
    }

    template<typename T>
    ListNode<T> RoundedLinkedList<T>::getEntry() {
        return *_entry;
    }

    template<typename T>
    size_t RoundedLinkedList<T>::size() {
        return _count;
    }

    template<typename T>
    T RoundedLinkedList<T>::pop() {
        if (_count == 0) throw std::range_error("This list is an  empty list!");
        ListNode<T> *currentNode = _entry;
        if (_count == 1) {
            // If there are only one element, just delete it and set it to null pointer.
            ListNode<T> *onlyChild = _entry;
            T element = onlyChild->data;
            _entry = nullptr;
            delete onlyChild;
            _count--;
            return element;
        } else {
            for (size_t i = 0; i < _count - 2; ++i) {
                currentNode = currentNode->next;
            }
            T element = currentNode->next->data;
            ListNode<T> *deleteTarget = currentNode->next;
            currentNode->next = currentNode->next->next;
            delete deleteTarget;
            _count--;
            return element;
        }
    }

    template<typename T>
    void RoundedLinkedList<T>::insert(size_t pos, T element) {
        // Different from normal linked list, as the list is rounded, there will be no upper limit for the position,
        // for example, if a list have 5 elements, give the position 7 will be seen as position 2.
        auto *newElement = new ListNode<T>(element, nullptr);
        ListNode<T> *currentNode = _entry;
        for (size_t i = 0; i < pos - 1; ++i) {
            currentNode = currentNode->next;
        }
        if (pos == 0) push_front(element); else {
            newElement->next = currentNode->next;
            currentNode->next = newElement;
        }
        _count++;
    }


    template<typename T>
    void RoundedLinkedList<T>::remove(size_t pos) {
        // Different from normal linked list, as the list is rounded, there will be no upper limit for the position,
        // for example, if a list have 5 elements, give the position 7 will be seen as position 2.
        ListNode<T> *currentNode = _entry;
        if (pos == 0) {
            remove_front();
            _count--;
            return;
        }
        // if (pos == 1) {
        //     auto *deleteTarget = currentNode->next;
        //     currentNode->next = currentNode->next->next;
        //     delete deleteTarget;
        //     return;
        // }
        if (_count == 1) {
            this->pop();
            return;
        }
        for (int i = 0; i < pos - 1; ++i) {
            currentNode = currentNode->next;
        }
        if (pos % _count == 0) {
            // Ensure that if the list is not empty there is always a real entry.
            // If the entry will be deleted, move the entry indicator to the next element.
            _entry = _entry->next;
        }
        auto *deleteTarget = currentNode->next;
        currentNode->next = currentNode->next->next;
        delete deleteTarget;
        _count--;
    }

    template<typename T>
    void RoundedLinkedList<T>::remove_back() {
        this->pop();
    }

    template<typename T>
    void RoundedLinkedList<T>::remove_front() {
        if (_count <= 1) {this->pop(); return;}
        ListNode<T> *currentNode = _entry;
        for (size_t i = 0; i < _count - 1; ++i) {
            currentNode = currentNode->next;
        }
        ListNode<T> *deleteTarget = currentNode->next;
        currentNode->next = currentNode->next->next;
        _entry = _entry->next;
        delete deleteTarget;
        _count--;
    }

    template<typename T>
    std::vector<T> RoundedLinkedList<T>::toVector() {
        std::vector<T> result {};
        ListNode<T> *currentNode = _entry;
        for (size_t i = 0; i < _count; ++i) {
            result.push_back(currentNode->data);
            currentNode = currentNode->next;
        }
        return result;
    }
}

#endif //WHUDATASTRUCTUREEXP04_TASK06_H
