//
// Created by Ostrichbeta Chan on 2022/03/24.
//

#ifndef WHUDATASTRUCTUREEXP04_TASK03_H
#define WHUDATASTRUCTUREEXP04_TASK03_H

#include "../TaskTemplate.h"

void task03Entry();
const TaskTemplate task03("Single Direction Linked List", "Made a single direction linked list on your own.", task03Entry);

#endif //WHUDATASTRUCTUREEXP04_TASK03_H
