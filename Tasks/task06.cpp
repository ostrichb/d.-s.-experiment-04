//
// Created by Ostrichbeta Chan on 2022/03/30.
// Task 06: Headless Linked List
// Description: Create a headless linked list.
//
#include "task06.h"

void task06Entry(){
    using namespace t6;
    RoundedLinkedList<int> newList{};
    newList.push_front(3);
    newList.push_back(6);
    newList.push_back(7);
    newList.push_back(8);
    newList.remove(2);
    newList.remove(2);
    newList.remove(2);
    newList.remove(2);
    std::cout << "Test Ended, as there are no outputs, please set breakpoints manually." << std::endl;
}

