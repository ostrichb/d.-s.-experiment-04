//
// Created by Ostrichbeta Yick-Ming Chan on 3/22/22.
//

#ifndef WHUDATASTRUCTUREEXP04_TASK02_H
#define WHUDATASTRUCTUREEXP04_TASK02_H

#include "../TaskTemplate.h"

void task02Entry();
const TaskTemplate task02("Hand-made sequenced list VS built-in vectors", "Compare your own structure with the built-in vector list", task02Entry);

#endif //WHUDATASTRUCTUREEXP04_TASK02_H
