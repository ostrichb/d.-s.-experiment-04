//
// Created by Ostrichbeta Chan on 2022/03/24.
//

#ifndef WHUDATASTRUCTUREEXP04_TASK04_H
#define WHUDATASTRUCTUREEXP04_TASK04_H

#include "../TaskTemplate.h"

void task04Entry();
const TaskTemplate task04("Handmade Linked List VS Built-in std::list", "Compare your own structure with the built-in list.", task04Entry);

#endif //WHUDATASTRUCTUREEXP04_TASK04_H
