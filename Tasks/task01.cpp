//
// Created by Ostrichbeta Yick-Ming Chan on 3/22/22.
// Task 01: Sequenced List Library
// Description: Make your own sequenced list library on your own.
// P.S.: Since this task requires to create a static library, the codes put here would only be used for reference instead of execution.
//

#include <iostream>
#include <sstream>

void task01Entry(){
    std::cout << "Since this task requires to create a static library, the codes put here would only be used for reference instead of execution." << std::endl;
}

// Original filename: library.h

template <typename T>
class SequencedList {
private:
    T* data_ptr = nullptr;
    unsigned int _count = 0;
public:
    SequencedList(): _count(0), data_ptr(nullptr) {};
    [[maybe_unused]] explicit SequencedList(unsigned int length);
    SequencedList(unsigned int length, T element);

    void push_back(T element);
    void push_back(T element, unsigned int times);

    void insert(T element, unsigned int pos);
    void insert(T element, unsigned int pos, unsigned int times);
    void set(unsigned int index, T element);

    void remove(size_t index, size_t range = 1);

    // Used for arguments of iterating functions
    T* begin();
    T* end();

    unsigned int size();
    void operator+(SequencedList &other);

    T operator[](int index);
    ~SequencedList();
};


template<typename T>
[[maybe_unused]] SequencedList<T>::SequencedList(unsigned int length) {
    if (length == 0) {
        SequencedList<T>();
    } else {
        data_ptr = new T[length];
        _count = length;
    }
}

template<typename T>
SequencedList<T>::SequencedList(unsigned int length, T element) {
    if (length == 0) {
        SequencedList<T>();
    } else {
        data_ptr = new T[length];
        _count = length;
        for (int i = 0; i < length; i++) {
            data_ptr[i] = element;
        }
    }
}

template<typename T>
SequencedList<T>::~SequencedList() {
    if (_count != 0) {
        delete [] data_ptr;
    }
}

template<typename T>
void SequencedList<T>::push_back(T element) {
    T* result = nullptr;
    result = new T[_count + 1];
    if (_count != 0) {
        for (int i = 0; i < _count; ++i) {
            result[i] = data_ptr[i];
        }
        // Delete the old pointer to avoid stack overflow or memory leak
        delete [] data_ptr;
    }
    result[_count] = element;
    // Link the generated memory space to the old variable.
    data_ptr = result;
    _count++;
}

template<typename T>
void SequencedList<T>::push_back(T element, unsigned int times) {
    for (int i = 0; i < times; ++i) {
        push_back(element);
    }
}

template<typename T>
void SequencedList<T>::insert(T element, unsigned int pos) {
    if (pos >= _count) {
        throw std::out_of_range("Target position is larger than the current length!");
    } else {
        T* result = new T[_count + 1];
        //Copy other elements
        for (unsigned int i = 0; i < pos; ++i) {
            result[i] = data_ptr[i];
        }
        result[pos] = element;
        for (unsigned int i = pos + 1; i < _count + 1; ++i) {
            result[i] = data_ptr[i - 1];
        }
        delete [] data_ptr;
        data_ptr = result;
        _count++;
    }
}

template<typename T>
void SequencedList<T>::insert(T element, unsigned int pos, unsigned int times) {
    if (times == 0) return;
    for (unsigned int i = pos; i < pos + times; ++i) {
        insert(element, i);
    }
}

template<typename T>
void SequencedList<T>::operator+(SequencedList &other) {
    for (T& item : other) {
        push_back(item);
    }
}

template<typename T>
unsigned int SequencedList<T>::size() {
    return _count;
}

template<typename T>
T SequencedList<T>::operator[](int index) {
    if (index >= _count) throw std::out_of_range("Target position is larger than the current length!");
    return data_ptr[index];
}

template<typename T>
void SequencedList<T>::set(unsigned int index, T element) {
    if (index >= _count) throw std::out_of_range("Target position is larger than the current length!");
    data_ptr[index] = element;
}

template<typename T>
T *SequencedList<T>::begin() {
    return data_ptr;
}

template<typename T>
T *SequencedList<T>::end() {
    return data_ptr + _count;
}

template<typename T>
void SequencedList<T>::remove(size_t index, size_t range) {
    if (range == 0) return;
    std::stringstream errorOutput;
    errorOutput << "範囲外！指定された位置 " << index << " は " << _count << " より大きい。";
    if (index > _count || index + range - 1 > _count) throw std::range_error(errorOutput.str());
    _count -= range;
    for (size_t i = index; i < _count; ++i) {
        data_ptr[i] = data_ptr[i + range];
        std::vector<T> temp(data_ptr, data_ptr + _count);
    }
    T* newDataPtr = new T[_count];
    for (size_t i = 0; i < _count; ++i) {
        newDataPtr[i] = data_ptr[i];
    }
    delete [] data_ptr ; // Delete the old space in case of stack overflow and memory leak.
    data_ptr = newDataPtr;
}
