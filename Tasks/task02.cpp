//
// Created by Ostrichbeta Yick-Ming Chan on 3/22/22.
// Task 02: Hand-made sequenced list VS built-in vectors
// Description: Compare your own structure with the built-in vector list.
//

#include <iostream>
#include <vector>
#include <random>

#include "../headers/library.h"

void task02Entry(){
    // Preparations for random number generation
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution dis(-999, 999);

    SequencedList<int> handMade(30);
    std::cout << "Fill numbers into handmade list: ";
    for (int &item : handMade) {
        item = dis(gen); // Set a random number between -999 and 999 to the element.
    }

    std::cout << "OK. \n Linear read test:" << std::endl;
    std::cout << "The elements in handmade list are: ";
    for (const int &item : handMade) {
        std::cout << item << " ";
    }
    std::cout << std::endl;

    // Copy the elements in handmade list to a new vector list
    std::vector<int> builtIn(handMade.begin(), handMade.end());
    // Since the whole list can be clearly seen in debug window, I will not write an extra print function for this.
    // Please set a breakpoint manually to investigate right here.


    // Add an element
    int newElement = dis(gen);
    std::cout << "Generated a new element: " << newElement << "." << std::endl;

    handMade.push_back(newElement);
    builtIn.push_back(newElement);

    std::cout << "The elements in handmade list are: ";
    for (const int &item : handMade) {
        std::cout << item << " ";
    }
    std::cout << std::endl;

    // Since the whole list can be clearly seen in debug window, I will not write an extra print function for this.
    // Please set a breakpoint manually to investigate right here.

    // Delete an element
    handMade.remove(2, 5);
    builtIn.erase(builtIn.begin() + 2, builtIn.begin() + 7);

    std::cout << "The elements in handmade list are: ";
    for (const int &item : handMade) {
        std::cout << item << " ";
    }
    std::cout << std::endl;

    // Since the whole list can be clearly seen in debug window, I will not write an extra print function for this.
    // Please set a breakpoint manually to investigate right here.

    // Sorting Test
    std::sort(handMade.begin(), handMade.end(), std::less_equal());
    std::sort(builtIn.begin(), builtIn.end(), std::less_equal());

    std::cout << "The elements in handmade list are: ";
    for (const int &item : handMade) {
        std::cout << item << " ";
    }
    std::cout << std::endl;

    // Since the whole list can be clearly seen in debug window, I will not write an extra print function for this.
    // Please set a breakpoint manually to investigate right here.
}

