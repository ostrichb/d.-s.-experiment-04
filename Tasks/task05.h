//
// Created by Ostrichbeta Chan on 2022/03/24.
//

#ifndef WHUDATASTRUCTUREEXP04_TASK05_H
#define WHUDATASTRUCTUREEXP04_TASK05_H

#include "../TaskTemplate.h"

void task05Entry();
const TaskTemplate task05("Joseph's Ring", "Use appropriate algorithms to solve any Josephus's ring question.", task05Entry);

#endif //WHUDATASTRUCTUREEXP04_TASK05_H
