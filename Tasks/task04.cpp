//
// Created by Ostrichbeta Chan on 2022/03/24.
// Task 04: Handmade linked list VS built-in std::list
// Description: Compare your own structure with the built-in list.

#include <iostream>
#include <vector>
#include <list>
#include <random>


#include "library.h"

void task04Entry(){
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution dis(-99, 99);

    // Use a vector to make a reference.
    std::vector<int> ref;
    for (size_t i = 0; i < 5; i++) {
        ref.push_back(dis(gen));
    }

    LinkedList<int> handMade(ref);
    std::list<int> builtIn(ref.begin(), ref.end());

    builtIn.push_back(7);
    handMade.push_back(7);

    builtIn.push_front(9);
    handMade.push_front(9);

    handMade.pop_front();
    builtIn.pop_front();

    handMade.pop_back();
    builtIn.pop_back();

    return;
};
