//
// Created by Ostrichbeta Chan on 2022/03/24.
// Task 03: Single Direction Linked List
// Description: Made a single direction linked list on your own.
// P.S.: Since this task requires to create a static library, the codes put here would only be used for reference instead of execution.
//

// Original file: library.h

#include <vector>
#include <iostream>

void task03Entry(){
    std::cout << "Since this task requires to create a static library, the codes put here would only be used for reference instead of execution." << std::endl;
}

template<typename T>
class listNode {
public:
    T data;
    listNode<T> *next = nullptr;
};

template<typename T>
class LinkedList {
private:
    size_t _count = 0;
    listNode<T> *first = nullptr;
public:
    LinkedList();
    LinkedList(size_t size, T element);
    LinkedList(std::vector<T> vectList);

    template <class Iterator>
    LinkedList(Iterator begin, Iterator end);


    void insert(size_t pos, T element, size_t time = 1);
    void push_front(T element);
    void push_back(T element);
    void pop_front();
    void pop_back();


    listNode<T> getFirst();
    listNode<T> getLast();
    std::vector<T> toVector();
};

template<typename T>
LinkedList<T>::LinkedList() {
    _count = 0;
    first = nullptr;
}

template<typename T>
LinkedList<T>::LinkedList(size_t size, T element) {
    for (size_t i = 0; i < size; ++i) {
        this->push_front(element);
    }
}

template<typename T>
LinkedList<T>::LinkedList(std::vector<T> vectList) {
    for (int i = vectList.size() - 1; i >= 0; --i) {
        this->push_front(vectList[i]);
    }
}

template<typename T>
template<class Iterator>
LinkedList<T>::LinkedList(Iterator begin, Iterator end) {
    LinkedList<T> result;
    for (T* current = end - 1; current >= begin; current--){
        result.push_front(*current);
    }
    return result;
}

template<typename T>
listNode<T> LinkedList<T>::getLast() {
    listNode<T> *currentNode = first;
    while (currentNode->next != nullptr) currentNode = currentNode->next;
    return currentNode;
}

template<typename T>
listNode<T> LinkedList<T>::getFirst() {
    return *first;
}

template<typename T>
void LinkedList<T>::push_front(T element) {
    if (_count == 0) {
        // If there are no elements in the linked list, modify the "first" value.
        first = new listNode<T> {element, nullptr};
    } else {
        listNode<T> *newHead = new listNode<T> {element, nullptr};
        newHead->next = first;
        first = newHead;
    }
    _count++;
}

template<typename T>
void LinkedList<T>::push_back(T element) {
    listNode<T> *currentNode = first;
    while (currentNode->next != nullptr) currentNode = currentNode->next;
    // currentNode now reaches the end of the list.
    listNode<T> *newTail = new listNode<T> {element, nullptr};
    currentNode->next = newTail;
    _count++;
}

template<typename T>
void LinkedList<T>::pop_front() {
    listNode<T> *oldptr = first;
    first = first->next;
    delete oldptr;
    _count--;
}

template<typename T>
void LinkedList<T>::pop_back() {
    listNode<T> *currentNode = first;
    for (size_t i = 0; i < _count - 2; ++i) {
        currentNode = currentNode->next;
    }
    delete currentNode->next;
    currentNode->next = nullptr;
    _count--;
}

template<typename T>
void LinkedList<T>::insert(size_t pos, T element, size_t time) {
    // Insert a new node BEFORE the given position.
    listNode<T> *currentNode = first;
    if (pos > _count) throw std::range_error("Position out of range!");
    if (time == 1) {
        for (size_t i = 0; i < pos - 1; ++i) {
            currentNode = currentNode->next;
        }
        // Insertion part
        if (pos == 0) push_front(element); else {
            listNode<T> newNode = {element, currentNode->next};
            currentNode->next = &newNode;
        }
    }
}

template<typename T>
std::vector<T> LinkedList<T>::toVector() {
    std::vector<T> result;
    for (listNode<T> currentNode = first; currentNode.next != nullptr; currentNode = *(currentNode.next)) {
        result.push_back(currentNode.data);
    }
    return result;
}

