#ifndef EXPLIB03_LIBRARY_H
#define EXPLIB03_LIBRARY_H

#include <sstream>
#include <vector>
#include <iostream>

template <typename T>
class SequencedList {
private:
    T* data_ptr = nullptr;
    unsigned int _count = 0;
public:
    SequencedList(): _count(0), data_ptr(nullptr) {};
    [[maybe_unused]] explicit SequencedList(unsigned int length);
    SequencedList(unsigned int length, T element);

    void push_back(T element);
    void push_back(T element, unsigned int times);

    void insert(T element, unsigned int pos);
    void insert(T element, unsigned int pos, unsigned int times);
    void set(unsigned int index, T element);

    void remove(size_t index, size_t range = 1);

    // Used for arguments of iterating functions
    T* begin();
    T* end();

    unsigned int size();
    void operator+(SequencedList &other);

    T operator[](int index);
    ~SequencedList();
};


template<typename T>
[[maybe_unused]] SequencedList<T>::SequencedList(unsigned int length) {
    if (length == 0) {
        SequencedList<T>();
    } else {
        data_ptr = new T[length];
        _count = length;
    }
}

template<typename T>
SequencedList<T>::SequencedList(unsigned int length, T element) {
    if (length == 0) {
        SequencedList<T>();
    } else {
        data_ptr = new T[length];
        _count = length;
        for (int i = 0; i < length; i++) {
            data_ptr[i] = element;
        }
    }
}

template<typename T>
SequencedList<T>::~SequencedList() {
    if (_count != 0) {
        delete [] data_ptr;
    }
}

template<typename T>
void SequencedList<T>::push_back(T element) {
    T* result = nullptr;
    result = new T[_count + 1];
    if (_count != 0) {
        for (int i = 0; i < _count; ++i) {
            result[i] = data_ptr[i];
        }
        // Delete the old pointer to avoid stack overflow or memory leak
        delete [] data_ptr;
    }
    result[_count] = element;
    // Link the generated memory space to the old variable.
    data_ptr = result;
    _count++;
}

template<typename T>
void SequencedList<T>::push_back(T element, unsigned int times) {
    for (int i = 0; i < times; ++i) {
        push_back(element);
    }
}

template<typename T>
void SequencedList<T>::insert(T element, unsigned int pos) {
    if (pos >= _count) {
        throw std::out_of_range("Target position is larger than the current length!");
    } else {
        T* result = new T[_count + 1];
        //Copy other elements
        for (unsigned int i = 0; i < pos; ++i) {
            result[i] = data_ptr[i];
        }
        result[pos] = element;
        for (unsigned int i = pos + 1; i < _count + 1; ++i) {
            result[i] = data_ptr[i - 1];
        }
        delete [] data_ptr;
        data_ptr = result;
        _count++;
    }
}

template<typename T>
void SequencedList<T>::insert(T element, unsigned int pos, unsigned int times) {
    if (times == 0) return;
    for (unsigned int i = pos; i < pos + times; ++i) {
        insert(element, i);
    }
}

template<typename T>
void SequencedList<T>::operator+(SequencedList &other) {
    for (T& item : other) {
        push_back(item);
    }
}

template<typename T>
unsigned int SequencedList<T>::size() {
    return _count;
}

template<typename T>
T SequencedList<T>::operator[](int index) {
    if (index >= _count) throw std::out_of_range("Target position is larger than the current length!");
    return data_ptr[index];
}

template<typename T>
void SequencedList<T>::set(unsigned int index, T element) {
    if (index >= _count) throw std::out_of_range("Target position is larger than the current length!");
    data_ptr[index] = element;
}

template<typename T>
T *SequencedList<T>::begin() {
    return data_ptr;
}

template<typename T>
T *SequencedList<T>::end() {
    return data_ptr + _count;
}

template<typename T>
void SequencedList<T>::remove(size_t index, size_t range) {
    if (range == 0) return;
    std::stringstream errorOutput;
    errorOutput << "範囲外！指定された位置 " << index << " は " << _count << " より大きい。";
    if (index > _count || index + range - 1 > _count) throw std::range_error(errorOutput.str());
    _count -= range;
    for (size_t i = index; i < _count; ++i) {
        data_ptr[i] = data_ptr[i + range];
        std::vector<T> temp(data_ptr, data_ptr + _count);
    }
    T* newDataPtr = new T[_count];
    for (size_t i = 0; i < _count; ++i) {
        newDataPtr[i] = data_ptr[i];
    }
    delete [] data_ptr ; // Delete the old space in case of stack overflow and memory leak.
    data_ptr = newDataPtr;
}

template<typename T>
class listNode {
public:
    T data;
    listNode<T> *next = nullptr;
};

template<typename T>
class LinkedList {
private:
    size_t _count = 0;
    listNode<T> *first = nullptr;
public:
    LinkedList();
    LinkedList(size_t size, T element);
    LinkedList(std::vector<T> vectList);

    template <class Iterator>
    LinkedList(Iterator begin, Iterator end);


    void insert(size_t pos, T element, size_t time = 1);
    void push_front(T element);
    void push_back(T element);
    void pop_front();
    void pop_back();


    listNode<T> getFirst();
    listNode<T> getLast();
    std::vector<T> toVector();
};

template<typename T>
LinkedList<T>::LinkedList() {
    _count = 0;
    first = nullptr;
}

template<typename T>
LinkedList<T>::LinkedList(size_t size, T element) {
    for (size_t i = 0; i < size; ++i) {
        this->push_front(element);
    }
}

template<typename T>
LinkedList<T>::LinkedList(std::vector<T> vectList) {
    for (int i = vectList.size() - 1; i >= 0; --i) {
        this->push_front(vectList[i]);
    }
}

template<typename T>
template<class Iterator>
LinkedList<T>::LinkedList(Iterator begin, Iterator end) {
    LinkedList<T> result;
    for (T* current = end - 1; current >= begin; current--){
        result.push_front(*current);
    }
    return result;
}

template<typename T>
listNode<T> LinkedList<T>::getLast() {
    listNode<T> *currentNode = first;
    while (currentNode->next != nullptr) currentNode = currentNode->next;
    return currentNode;
}

template<typename T>
listNode<T> LinkedList<T>::getFirst() {
    return *first;
}

template<typename T>
void LinkedList<T>::push_front(T element) {
    if (_count == 0) {
        // If there are no elements in the linked list, modify the "first" value.
        first = new listNode<T> {element, nullptr};
    } else {
        listNode<T> *newHead = new listNode<T> {element, nullptr};
        newHead->next = first;
        first = newHead;
    }
    _count++;
}

template<typename T>
void LinkedList<T>::push_back(T element) {
    listNode<T> *currentNode = first;
    while (currentNode->next != nullptr) currentNode = currentNode->next;
    // currentNode now reaches the end of the list.
    listNode<T> *newTail = new listNode<T> {element, nullptr};
    currentNode->next = newTail;
    _count++;
}

template<typename T>
void LinkedList<T>::pop_front() {
    listNode<T> *oldptr = first;
    first = first->next;
    delete oldptr;
    _count--;
}

template<typename T>
void LinkedList<T>::pop_back() {
    listNode<T> *currentNode = first;
    for (size_t i = 0; i < _count - 2; ++i) {
        currentNode = currentNode->next;
    }
    delete currentNode->next;
    currentNode->next = nullptr;
    _count--;
}

template<typename T>
void LinkedList<T>::insert(size_t pos, T element, size_t time) {
    // Insert a new node BEFORE the given position.
    listNode<T> *currentNode = first;
    if (pos > _count) throw std::range_error("Position out of range!");
    if (time == 1) {
        for (size_t i = 0; i < pos - 1; ++i) {
            currentNode = currentNode->next;
        }
        // Insertion part
        if (pos == 0) push_front(element); else {
            listNode<T> newNode = {element, currentNode->next};
            currentNode->next = &newNode;
        }
    }
}

template<typename T>
std::vector<T> LinkedList<T>::toVector() {
    std::vector<T> result;
    for (listNode<T> currentNode = first; currentNode.next != nullptr; currentNode = *(currentNode.next)) {
        result.push_back(currentNode.data);
    }
    return result;
}


#endif //EXPLIB03_LIBRARY_H
