# Data Structure Experiment 04

> 註：題目的任務經會以中文寫出，但程式碼將全部以英文寫出。

本次實驗一共包含 **6** 個題目。

1. 在既有“類庫”型項目 dsa 中，新建自訂的順序表SequencedList 模組（.cpp 和.h 文件），編程實現順序表類模板。
2. 創建項目ex4p2，在 main 函數中利用順序表類SequencedList，定義和初始化一個int 類型的線性表，在表中添加（push_back）和插入（insert）新的元素。檢查自己的設計與標準庫的vector 模板類在基本性質上是否相同。
3. 在既有“類庫”型項目 dsa 中，新建自訂的單向鍊表SingleLinkedList模組（.cpp 和.h 文件），編程實現單向鍊表類模板和結點類模板。
4. 創建項目ex4p4，在 main 函數中利用單向鍊表類SingleLinkedList，定義和初始化一個Student 類型的線性鍊表，在表中添加（push_back）和插入（insert）新的元素。檢查自己的設計與標準庫的list 類模板在基本性質上是否相同。
5. 創建項目ex4p5，選擇一種合適的線性表類求解約瑟夫（Josephus）環問題。
6. 設計和實現一種不含頭結點的單向鍊表類。與不含頭結點的設計相比較，具有頭結點的單向鍊表類的設計，在一些操作算法的實現上帶來了方便，例如無需判斷鍊表是否是單數據結點的情況。

## 提示

- 本實驗所用的原始碼所用的撰寫標準為 C++23 ，不保證其在舊標準的 C++ 中正常運行。(尤其是 C++17 及以下)
- 本實驗所用的 IDE 為 Clion 2021.3.3 於 macOS Monterey 12.3 中運行，不保證其在其他系統中的相容性。

## Caution

- The code standard of the source code used in this experiment is C++23,  its performance in the old standard C++ is not guaranteed. (Especially C++17 and below)
- The IDE used in this experiment is Clion 2021.3.3, which runs on macOS Monterey 12.3, its compatibility in other systems is not guaranteed.