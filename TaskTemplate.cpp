//
// Created by Ostrichbeta Yick-Ming Chan on 3/4/22.
//

#include "TaskTemplate.h"

#include <utility>

std::string TaskTemplate::getTitle() {
    return title;
}

std::string TaskTemplate::getDescription() {
    return description;
}

TaskTemplate::TaskTemplate(std::string ttl, std::string desc, void (*ent)()) {
    title = std::move(ttl);
    description = std::move(desc);
    entry = ent;
}

